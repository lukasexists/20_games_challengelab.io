---
title: "Breakout Showcase"
anchor: "breakout_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* SereneJellyfish made a [web playable game](https://serenejellyfishgames.itch.io/beachside-breakout)

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/generic-breakout-clone)

* Martal made a [web playable game](https://martialis39.itch.io/brick-game)

* SDG Games made a video devlog:
  {{< youtube xbw1DKH0pqA >}}
  And here's the [source code](https://gitlab.com/20-games-in-30-days/breakout)

{{< include file="_parts/showcase_footer.md" type=page >}}
