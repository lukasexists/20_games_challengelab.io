---
title: "Pac_Man Showcase"
anchor: "pacman_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* Erip made a [web playable game](https://games.petzel.io/pacman/)

* SDG Games made a video devlog:
  {{< youtube 221sPOp_514 >}}
  And here's the [source code](https://gitlab.com/20-games-in-30-days/pac-man)

* More to come!

{{< include file="_parts/showcase_footer.md" type=page >}}
