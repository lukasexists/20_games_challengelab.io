---
title: "Flappy Bird Showcase"
anchor: "flappy_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* JimothyZee made a [web playable game](https://gx.games/games/dyizk2/flappy-eagle/) (only works on Opera browser)

* tnag552 made a [web playable game](https://txorimalo.itch.io/helloucar)

* Valumin made a [web playable game](https://valumin.itch.io/flappy-ufo)

* Nazorus made a [web playable game](https://nazorus.itch.io/jetcave)

* AllHeart made a [web playable game](https://gx.games/games/busvgk/flying-fish/tracks/c38f865d-9823-4744-8f00-95ff8f8737a6) (only works on Opera browser)

{{< include file="_parts/showcase_footer.md" type=page >}}
