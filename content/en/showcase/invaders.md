---
title: "Space Invaders Showcase"
anchor: "invaders_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* TomisGreen made a [web playable game](https://tomisgreen.itch.io/the-day-the-eldritch-came)

* SDG Games made a video devlog:
  {{< youtube kV5N86ULkpw >}}
  And here's the [source code](https://gitlab.com/20-games-in-30-days/space-invaders)

* More to come!

{{< include file="_parts/showcase_footer.md" type=page >}}
