---
title: "Jetpack Joyride Showcase"
anchor: "jetpack_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* JimothyZee made a [web playable game](https://gx.games/games/8vccin/hoverboard-joyride/) (only works on Opera browser)

* GameDevJourney made a video devlog
  {{< youtube bIySGIJCALk >}}
  The game can be downloaded [here](https://buzjr.itch.io/jetpack-joyride)

{{< include file="_parts/showcase_footer.md" type=page >}}
