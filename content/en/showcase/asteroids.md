---
title: "Asteroids Showcase"
anchor: "asteroids_show"
---

{{< include file="_parts/showcase_header.md" type=page >}}

* {SDG Games} This was my fourth game in the challenge, and my first particle effects!
  {{< youtube zX4clPDU9cA >}}
  And here's the [source code](https://gitlab.com/20-games-in-30-days/asteroids)

* More to come!

{{< include file="_parts/showcase_footer.md" type=page >}}