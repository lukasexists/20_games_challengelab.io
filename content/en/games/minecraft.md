---
title: "Minecraft"
anchor: "minecraft"
weight: 100
---

Minecraft is the single best-selling video game of all time. It came out it 2011, and combined the voxel-based mining of Infiniminer with iconic graphics and a dynamic difficulty curve based around day-night cycles.

Minecraft helped popularize voxel based world generation. Its infinite world is generated dynamically, one chunk at a time.

| ***Difficulty*** |                                                                                 |
| :---             | :---                                                                            |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}}                                         | 

### Goal:
* Create a first-person player controller. The player should be able to look in any direction, jump, walk, and strafe.
* Create a voxel-based world. Use procedural generation and noise to create hills, trees, and caves.
{{< hint type=tip title="What are voxels?">}}
Minecraft uses cubes to generate its world. For performance reasons, Minecraft will create one mesh per chunk to send to the GPU. The inside of the world isn't actually rendered, it's completely hollow.

Once you know how to make Minecraft, creating a "smooth" voxel terrain is fairly easy as well. You just have to smooth out the bumps between cubes by moving the corners of the cubes up or down a bit.
{{< /hint >}}
* The player should be able to remove blocks from the terrain, and add new blocks.

### Stretch goal:
* Add the option to save your game and load the world from a file. (You might want to re-generate parts of the world, but store player-made changes somewhere)
* There are a lot of potential features to Minecraft, so feel free to add one or more on top of the basic "mining and placing blocks" part. Some potential features to add:
  * An inventory and crafting system.
  * A day/night cycle.
  * Multiplayer support.
  * Water and fluid simulation.
  * Enemies and combat.

{{< expand "Showcase" >}} {{< include file="showcase/empty.md" type=page >}} {{< /expand >}}
