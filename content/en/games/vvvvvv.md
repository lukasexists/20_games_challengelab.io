---
title: "VVVVVV"
anchor: "vvvvvv"
weight: 62
---

VVVVVV is a retro puzzle-platformer. It came out in 2010, but it could have been at home in early 8-bit games. Instead of jumping, the player controls gravity, allowing the protagonist to fall upwards or downwards. The game is a great example of designing within constraints. Instead of focusing on "modern" graphics and sound, the game limits itself to simple sprites and chip-tune music.

| ***Difficulty*** |                                         |
| :---             | :---                                    |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} | 

### Goal:
* Create a 2D character controller. The player should be able to walk left or right. They cannot jump, instead gravity will reverse directions. Gravity will only change if the player is standing on a surface.
* Create a puzzle level. Each level will be made up of multiple rooms.
  * Rooms can contain spikes, moving platforms, moving enemies, or dissolving platforms (that disappear a second after they player stands on them)
  * The player can exit the room in any direction, and will walk or fall into an adjacent room when they do so.
  * Each room will contain one or more checkpoints. When a player touches a checkpoint, that checkpoint will light up. Players will return to the illuminated checkpoint when they die (even if it is in another room).
* Make some music and sound effects! (If you want to learn to make your own music, then take this opportunity to learn about chip-tunes, and compose something fun!)

### Stretch goal:
* VVVVVV is a simple concept, but contains a lot of polish, refinement, and charm. Add a unique theme to each room in your level.<br><br>
  Try to make each room (or set of rooms) into a complete and unique puzzle. Share your game with friends or on the [community discord](https://discord.gg/mBGd9hahZv), and ask for level design feedback. Feel free to iterate a few times until the difficulty feels just right.

{{< expand "Showcase" >}} {{< include file="showcase/empty.md" type=page >}} {{< /expand >}}
