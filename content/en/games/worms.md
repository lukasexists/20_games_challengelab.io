---
title: "Worms"
anchor: "worms"
weight: 70
---

Worms is a turn-based artillery game from 1995. In the game, two or more teams of worms take turns shooting each other with various weapons, including rockets, grenades, dynamite (and, of course, the Holy Hand Grenade.) The game featured fully destructible 2D terrain, with each weapon leaving a large circular crater in the landscape. By the end of most matches, the level is an unrecognizable mess of holes and caverns.

| ***Difficulty*** |                                                             |
| :---             | :---                                                        |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} | 

### Goal:
* Create a 2D game world with fully destructible terrain. The world should be an organic shape (as opposed to a tile set), and you should be able to cut holes into it, revealing the background.
* Create the worm characters. They are 2D characters that can walk, jump, and shoot.
* You should make weapon selection menu and give the worms some weapons. At minimum, you should give them a bazooka and a throwable grenade. Weapons will create a circular explosion when detonated. Worms caught in the explosion radius will be damaged. Terrain in the explosion radius will also be destroyed, leaving a hole.
* Create a game menu, turn system, and UI. Players will take turns controlling a worm on their team. The turn ends when the worm fires a weapon. Worms will take damage from weapons, and will die when their health reaches 0 (or if they fall off of the map). Dead worms will explode, damaging any nearby worms.

### Stretch goal:
* Create the Ninja rope (a grappling hook that allows worms to swing side to side and extend/retract the rope to move)
* Give the worms a jetpack to allow them to move from place to place.

{{< expand "Showcase" >}} {{< include file="showcase/empty.md" type=page >}} {{< /expand >}}
