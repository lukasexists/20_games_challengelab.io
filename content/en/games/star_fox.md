---
title: "Star Fox"
anchor: "star_fox"
weight: 81
---

Star Fox was an incredibly ambitious rail shooter when it released on the SNES in 1993. Star Fox was the first Nintendo game to feature 3D polygonal graphics, thanks to a Super FX graphics chip that was included in the game cartridge itself. The cartridge contained more more processing power than the game console did. (Imaging buying a game that comes with its own dedicated graphics cards today!)

Star Fox is considered one of the most successful games of all time, and it became a long-running series. The graphics have greatly improved since the first game, so don't feel like you have to limit yourself to the polygon count of the original.

| ***Difficulty*** |                                                                                      |
| :---             | :---                                                                                 |
| Complexity       | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star-half" >}} |
| Scope            | {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}} {{< icon "star" >}}      | 

### Goal:
* Create the world for a 3D rail shooter. The player will travel in a 2D "box" through a 3D world. The movement should follow a predefined path, though the player can control the speed of movement.
  * You can make a space level - where there are only enemies, or you can add terrain and obstacles to avoid.
* Add a player ship. The player can move anywhere on the screen (in 2D space), but the larger movement is controlled by the level. You can add a boost and brake feature to speed up and slow down the player's movements.
* Add enemies to the game. Enemies can be be static, cross the player's path, or travel with the player. Enemies will shoot at the player (and the player can shoot back).
  * Feel free to group enemies together in different patterns. You can re-use patterns throughout the level.
* Add a score counter and life counter

### Stretch goal:
* Make a giant boss to fight at the end of the level! The boss should travel with the player until defeated, and can have multiple attack patterns and weak spots.
* Give the player an upgrade! Add a power-up that improves the speed or power of their laser.
* Give the player some bombs. They only get a few per level, but a bomb can clear most or all of the onscreen enemies at once.

{{< expand "Showcase" >}} {{< include file="showcase/empty.md" type=page >}} {{< /expand >}}
